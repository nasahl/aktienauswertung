# Aktienauswertung

Dieses Programm erleichtert die Auswertung vergangener Wertpapier-Trades.

Entwickelt habe ich das Programm für mein Flatex Konto.
Dort ist es möglich die Depotumsätze für vergangene Trades folgendermaßen zu speichern:
```
Konto&Depot -> Depotumsätze -> Konto wählen -> Zeitraum maximal einstellen -> Übernehmen -> [...] -> Export CSV
```

Dies dient als Input für das Programm. Dabei können auch mehrere CSV Dateien gespeichert werden.
Die Dateien müssen nur im gleichen Verzeichnis liegen, um vom Programm genutzt zu werden.

### Voraussetzung
Eine Installation von Java 8 (oder höher) muß über die Windows PATH Variable erreichbar sein.

### Anpassungen
Das Programm muss für das persönliche Depot und die individualisierte Ausgabe angepasst werden.
Dazu müssen die folgenden Konfigurationsfiles  modifiziert werden:

- `properties-isin2symbol.txt`: Hier muß ein Mapping **ISIN -> SYMBOL** für die Wertpapiere eingetragen werden, 
die aktuell noch im Depot liegen. Dies ist nötig, damit das Programm die aktuellen Kurse online ermitteln kann.  
Die Mappings findet man am besten über die 
[Yahoo Finance Seite](https://de.finance.yahoo.com/).

- `properties.txt`: Definiert alle Parameter, die für eine spezifische Ausführung nötig sind.
    - `tradeDir`: Definiert das Verzeichnis mit den enthaltenen csv Dateien
    - `kontoDir`: Definiert das Verzeichnis mit den enthaltenen csv Kontodateien zur zusätzlichen Auswertung der Dividendenzahlung (optional)
    - `investAmount`: Definiert den investierbaren Gesamtbetrag (optional)
    - `startDate`: Der Starttermin der Transaktionen für die Auswertung (optional). Damit kann der Auswertungszeitraum eingeschränkt werden
    - `tradeFee`: Die durchschnittliche Gebühr eines Trades (optional)
    - `enableFileCleanup=true`: Aktiviert das Löschen redundanter CSV Dateien (optional). Eine CSV Datei ist dann 
    redundant, wenn die Menge der erhaltenen Trades eine Teilmenge einer anderen CSV Datei ist
    - `verbose=true`: Aktiviert das Schreiben von Log-Ausgaben (optional)
  

Die Datei `properties.txt` kann umbenannt werden und dient als Eingabeparameter bei der Programmausführung.

Beispielaufruf:

```
aktienauswertung.bat myProperties.txt
```

### Auswertung
Als Ergebnis wird eine Tabelle angezeigt, die alle gehandelten Wertpapierpositionen mit den folgenden Eigenschaften anzeigt:

- `Name`: Der Name des Wertpapiers
- `#TAs`: Die Anzahl der Transaktionen (=Trades) die für dieses Wertpapier ausgeführt wurden
- `Kurs`: Der aktuelle Kurs des Wertpapiers. Der Kurs wird nur für die Wertpapiere angezeigt, deren Anzahl nicht 0 ist
- `Stück`: Die aktuell noch vorhandene Anzahl im Depot
- `Gesamtkosten`: Die Summe aller Kauftransaktionen minus die Summe aller Verkaufstransaktionen
- `Gewinn`: Der Kurswert (Anzahl x Kurs) des Wertpapiers minus die Gesamtkosten
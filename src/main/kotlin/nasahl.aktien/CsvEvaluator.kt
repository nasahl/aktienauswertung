package nasahl.aktien

/** Helps to interpret a csv file */
class CsvEvaluator(private val csvColumnNames: List<String>) {

    fun valueOf(valueParts: Array<String>, specificName: String): String {
        for ((i, value) in csvColumnNames.withIndex()) {
            if (value == specificName) {
                return valueParts[i]
            }
        }
        throw IllegalStateException("The expected column name $specificName can't be found in the csv file")
    }

    fun checkNames(vararg singleNames: String) {
        for (singleName in singleNames) {
            check(csvColumnNames.contains(singleName)) { "The column header '$singleName' can't be found in csv file" }
        }
    }

}
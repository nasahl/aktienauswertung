package nasahl.aktien

import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * Is responsible to read the csv konto file to evaluate the overall dividend
 */
class CsvKontoReader internal constructor(private val datumName: String, private val infoName: String,
                                          private val betragName: String, private val separator: String,
                                          private val dividendePatternString: String,
                                          private val datePatternString: String) {

    private val datePattern: DateTimeFormatter = DateTimeFormatter.ofPattern(datePatternString)

    @Throws(IOException::class)
    fun addDividends(dividendSet: MutableSet<Dividend>, csvFile: File, startDate: LocalDate?) {
        val lines = Files.readAllLines(csvFile.toPath(), StandardCharsets.ISO_8859_1)
        val names = listOf(*lines[0].split(separator).toTypedArray())
        val csvEval = CsvEvaluator(names)
        csvEval.checkNames(datumName, infoName, betragName)
        for (line in lines.subList(1, lines.size)) {
            val parts = line.split(separator).toTypedArray()
            val info = csvEval.valueOf(parts, infoName)
            if (info.contains(dividendePatternString)) {
                val dividend = Dividend(LocalDate.parse(csvEval.valueOf(parts, datumName), datePattern),
                        csvEval.valueOf(parts, betragName),
                        info)
                if (startDate == null || !dividend.datum.isBefore(startDate)) {
                    dividendSet.add(dividend)
                }
            }
        }
    }
}
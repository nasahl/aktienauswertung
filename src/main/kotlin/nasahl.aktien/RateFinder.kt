package nasahl.aktien

import java.io.FileInputStream
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap

/**
 * Is responsible to find and manage the rate for a specific share
 */
class RateFinder(private val config: Config) {
    private val propsIsin2symbol: Properties = Properties()
    private val rateOfIsin: MutableMap<String, Double?> = HashMap()
    private fun getRequiredSymbol(isin: String, name: String): String {
        val symbol = propsIsin2symbol.getProperty(isin)
                ?: throw IllegalStateException("The mapping for isin $isin ($name) is missing in file $propsFile")
        return symbol.replace("\\s+#.*$".toRegex(), "")
    }

    @Throws(IOException::class)
    fun load(isin: String, name: String) {
        config.printVerbose("Load rate for isin '$isin' ($name) ...")
        val symbol = getRequiredSymbol(isin, name)
        val currentPrice = PriceReader().getPrice(symbol)!!.currentPrice!!.raw
        config.printlnVerbose(" -> " + String.format("%.2f", currentPrice))
        rateOfIsin[isin] = currentPrice
    }

    fun getLoadedRate(isin: String): Double {
        return rateOfIsin[isin] ?: throw IllegalStateException("The mapping for isin $isin is not loaded")
    }

    companion object {
        // A file with a mapping isin -> symbol. Has to be maintained manually
        private const val propsFile = "properties-isin2symbol.txt"
    }

    init {
        // Load the property file
        try {
            FileInputStream(propsFile).use { `is` -> propsIsin2symbol.load(`is`) }
        } catch (exc: IOException) {
            throw IllegalStateException("Can't load '$propsFile': " + exc.message)
        }
    }
}
package nasahl.aktien

import nasahl.aktien.Utils.d2s
import java.io.File
import java.io.IOException
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*
import java.util.function.Consumer
import java.util.stream.Collectors
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
import kotlin.system.exitProcess

/**
 * Berechnet den Gewinn von Aktientrades über die Jahre.
 *
 * Basiert auf csv Files von Flatex:
 *
 *     Konto&Depot -> Depotumsätze -> Konto wählen -> Zeitraum maximal einstellen -> Übernehmen -> [...] -> Export CSV
 */
object Main {
    private var config: Config? = null
    private var rateFinder: RateFinder? = null

    /**
     * The reader schema for a flatex csv file extracted by
     *
     *     Konto&Depot -> Depotumsätze -> Konto wählen -> Zeitraum maximal einstellen -> Übernehmen -> [...] -> Export CSV
     */
    private val flatexReader = CsvReader("Nummer", "Buchtag", "ISIN",
            "Bezeichnung", "Nominal", "Kurs", ";", "dd.MM.yyyy")

    private val flatexKontoReader = CsvKontoReader("Buchtag", "Buchungsinformationen",
            "Betrag", ";", "Dividendenzahlung", "dd.MM.yyyy")

    @Throws(IOException::class)
    @JvmStatic
    fun main(args: Array<String>) {

        // 1. Evaluate the arguments ...
        if (args.isEmpty()) {
            usage()
            exitProcess(1)
        }

        // 2. Initialize config and rateFinder ...
        config = Config(args[0])
        rateFinder = RateFinder(config!!)

        // 3. Extract all trades from the csv files ...
        val trades = allTrades

        // 4. Extract the dividends
        val shares = transformToListByShares(trades)

        // 5. Transform the trades to a share-related format
        val dividends = extractDividends()

        // 6. Evaluate the shares and print them
        evalAndWriteShares(shares, dividends, trades[0].datum)
    }

    private fun extractDividends(): Set<Dividend>? {
        val kontoDir = config!!.kontoDirectory;
        if (kontoDir != null) {
            return allDividends(kontoDir)
        }
        return null
    }

    private fun usage() {
        println("Usage: aktienauswertung.bat <properties-file>")
        println()
        println("This program evaluates your depot trades.")
        println("Currently it only works with csv files stored ba a flatex depot.")
        println("Read the readme.md file for further information.")
        println()
        println("The easiest way to run this program is using windows explorer and pull over " +
                "the icon of your personally configured properties.txt file.")
    }

    @get:Throws(IOException::class)
    private val allTrades: List<Trade>
        private get() {
            val files = config!!.shareDirectory.listFiles { dir, name -> name.endsWith(".csv") }
            var tradesPerFile: MutableMap<File, List<Trade>> = TreeMap()
            var trades: MutableList<Trade> = ArrayList()
            for (file in files) {
                config?.printlnVerbose("Parse file ${file.name} ...")
                val fileTrades = compress(flatexReader.convert(file, config!!.startDate))
                tradesPerFile[file] = fileTrades
                trades.addAll(fileTrades)
            }
            trades = removeDuplicates(trades)
            trades.forEach(Consumer { config?.printlnVerbose("$it -> cost=${d2s(it.anzahl * it.kurs)}") })
            removeSuperfluousFiles(tradesPerFile)
            return trades
        }

    /** Removes files which have no additional info */
    private fun removeSuperfluousFiles(tradesPerFile: Map<File, List<Trade>>) {
        var files2Delete: MutableList<File> = ArrayList()
        for ((i, file1) in tradesPerFile.keys.withIndex()) {
            if (files2Delete.contains(file1)) {
                continue
            }
            for ((k, file2) in tradesPerFile.keys.withIndex()) {
                if (i != k && isContained(tradesPerFile[file1]!!, tradesPerFile[file2]!!)) {
                    files2Delete.add(file2)
                    if (config!!.enableFileCleanup) {
                        println("File ${file2.name} will be removed because it is superfluous ...");
                        file2.deleteOnExit();
                    } else {
                        println("File ${file2.name} can be removed! Set 'enableFileCleanup=true' to cleanup automatically");
                    }
                }
            }
        }
    }

    private fun isContained(superSet: List<Trade>, subSet: List<Trade>): Boolean {
        return subSet.stream().allMatch { superSet.contains(it) }
    }


    @Throws(IOException::class)
    private fun transformToListByShares(trades: List<Trade>): List<ShareData> {
        val tradeMap: MutableMap<String, MutableList<Trade>> = TreeMap()
        for (trade in trades) {
            val item = tradeMap.getOrDefault(trade.isin, ArrayList())
            item.add(trade)
            tradeMap[trade.isin] = item
        }
        val shares: MutableList<ShareData> = ArrayList()
        for (tradesOfIsin in tradeMap.values) {
            var totalCost = 0.0
            var nrShares = 0.0
            for (b in tradesOfIsin) {
                totalCost += b.totalCost()
                nrShares += b.anzahl
            }
            val isin = tradesOfIsin[0].isin
            val name = tradesOfIsin[0].name
            shares.add(ShareData(isin, name, tradesOfIsin.size, nrShares, totalCost))
            if (nrShares != 0.0) {
                rateFinder!!.load(isin, name)
            }
        }
        return shares
    }

    @Throws(IOException::class)
    private fun evalAndWriteShares(shares: List<ShareData>, dividends: Set<Dividend>?, firstTradeDate: LocalDate) {
        val writer = Writer(config!!)
        writer.headline("Betrachtete Transaktionen seit " + firstTradeDate.format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
                "Zeitpunkt der Auswertung: " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy - HH:mm")))
        writer.write("Name", "Kurs", "#TAs", "Stück", "Gesamtkosten", "Gewinn")
        var totalGain = 0.0
        var totalTAs = 0
        for (share in shares) {
            var currentRate: Double? = null
            val gain: Double
            if (share.nrShares == 0.0) {
                gain = -share.totalCost
            } else {
                val isin = share.isin
                currentRate = rateFinder!!.getLoadedRate(isin)
                checkNotNull(currentRate) { ("The current rate for ${share.name} ($isin) is missing") }
                gain = share.nrShares * currentRate - share.totalCost
            }
            writer.write(share.name, currentRate, share.nrTAs, share.nrShares, share.totalCost, gain)
            totalGain += gain
            totalTAs += share.nrTAs
        }
        writer.writeEmpty()
        writer.write("Gewinn", d2s(totalGain))
        val tradeFee = config!!.tradeFee
        if (tradeFee != null) {
            val totalTradeFee = -1 * totalTAs * tradeFee
            totalGain += totalTradeFee
            writer.writeEmpty()
            writer.write("Transaktionsgebuehren (${d2s(tradeFee)}/TA)", d2s(totalTradeFee))
            writer.write("Gewinn (ohne Gebuehren)", d2s(totalGain))
        }
        val kontoDir = config!!.kontoDirectory;
        if (dividends != null) {
            var dividendsSum: Double = 0.0
            for (dividend in dividends) {
                dividendsSum += Utils.s2d(dividend.betrag)
            }
            totalGain += dividendsSum
            writer.writeEmpty()
            writer.write("Dividenden (${dividends.size} TAs)", d2s(dividendsSum))
            writer.write("Gewinn (mit Dividenden)", d2s(totalGain))
        }

        val investAmount = config!!.investAmount
        if (investAmount != null) {
            writer.writeEmpty()
            val percGain = 100 * totalGain / investAmount
            writer.write("Proz. Gewinn auf investierte Summe $investAmount.-", d2s(percGain) + "%")
            val yearsPast = ChronoUnit.DAYS.between(firstTradeDate, LocalDate.now()).toDouble() / 365
            val percYearlyGain = calculateYearlyInterest(investAmount, investAmount + totalGain, yearsPast)
            writer.write("Proz. Gewinn auf Summe/Jahr", d2s(percYearlyGain) + "%")
        }
        writer.store()
    }

    fun allDividends(kontoDir: File): Set<Dividend> {
        var dividends: MutableSet<Dividend> = HashSet()
        val files = kontoDir.listFiles { dir, name -> name.endsWith(".csv") }
        for (file in files) {
            config?.printlnVerbose("Parse konto file ${file.name} ...")
            flatexKontoReader.addDividends(dividends, file, config!!.startDate)
        }
        return dividends
    }

    fun calculateYearlyInterest(amountBefore: Int, amountAfter: Double, durationInYears: Double): Double {
        return 100 * Math.pow(amountAfter / amountBefore, 1 / durationInYears) - 100
    }

    private fun compress(buchungen: List<Trade>): List<Trade> {
        val buchMap: MutableMap<String, Trade> = TreeMap()
        for (trade in buchungen) {
            val key = trade.uniqueKey()
            val stored = buchMap[key]
            if (stored == null) {
                buchMap[key] = trade
            } else {
                buchMap[key] = stored.add(trade)
            }
        }
        return buchMap.values.stream().sorted(Comparator.comparing(Trade::datum))
                .collect(Collectors.toList())
    }

    private fun removeDuplicates(buchungen: List<Trade>): MutableList<Trade> {
        val buchMap: MutableMap<String, Trade> = TreeMap()
        buchungen.forEach(Consumer { b: Trade -> buchMap.putIfAbsent(b.uniqueKey(), b) })
        val uniqueTrades = buchMap.values.stream().sorted(Comparator.comparing(Trade::datum))
                .collect(Collectors.toList())
        config?.printlnVerbose("Removed trade duplicates: " + (buchungen.size - uniqueTrades.size))
        return uniqueTrades
    }

    internal class ShareData(var isin: String, var name: String, var nrTAs: Int, var nrShares: Double, var totalCost: Double)
}
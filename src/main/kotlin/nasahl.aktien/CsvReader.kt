package nasahl.aktien

import nasahl.aktien.Utils.s2d
import java.io.File
import java.io.IOException
import java.nio.charset.StandardCharsets
import java.nio.file.Files
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Is responsible to read the csv file with the trades
 */
class CsvReader internal constructor(private val depotName: String, private val datumName: String,
                                     private val isinName: String, private val nameName: String,
                                     private val anzahlName: String, private val kursName: String,
                                     private val separator: String, datePatternString: String) {

    private val datePattern: DateTimeFormatter = DateTimeFormatter.ofPattern(datePatternString)

    @Throws(IOException::class)
    fun convert(csvFile: File, startDate: LocalDate?): List<Trade> {
        val trades: MutableList<Trade> = ArrayList()
        val lines = Files.readAllLines(csvFile.toPath(), StandardCharsets.ISO_8859_1)
        val names = listOf(*lines[0].split(separator).toTypedArray())
        val csvEval = CsvEvaluator(names)
        csvEval.checkNames(depotName, datumName, isinName, nameName, anzahlName, kursName)
        for (line in lines.subList(1, lines.size)) {
            val parts = line.split(separator).toTypedArray()
            val trade = Trade(csvEval.valueOf(parts, depotName),
                    LocalDate.parse(csvEval.valueOf(parts, datumName), datePattern),
                    csvEval.valueOf(parts, isinName),
                    csvEval.valueOf(parts, nameName).replace("\\s+".toRegex(), " "),
                    s2d(csvEval.valueOf(parts, anzahlName)),
                    s2d(csvEval.valueOf(parts, kursName)))
            if (startDate == null || !trade.datum.isBefore(startDate)) {
                trades.add(trade)
            }
        }
        return trades
    }
}
package nasahl.aktien

import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

class Config internal constructor(propFileName: String) {
    val shareDirectory: File
    var kontoDirectory: File? = null
    var startDate: LocalDate? = null
    private val isVerbose: Boolean
    var investAmount: Int? = null
    var tradeFee: Double? = null
    val enableFileCleanup: Boolean

    init {

        // 1. Load the property file
        val props = Properties()
        try {
            FileInputStream(propFileName).use { `is` -> props.load(`is`) }
        } catch (exc: IOException) {
            throw IllegalStateException("Can't load '${propFileName}': " + exc.message)
        }

        // 2. Set the share directory
        val tradeDir = props.getProperty("tradeDir")
                ?: throw IllegalStateException("The properties file has no attribute 'tradeDir'")
        shareDirectory = File(tradeDir)
        check(shareDirectory.isDirectory) { "Directory '$shareDirectory' doesn't exist" }

        // 3. Set the start date if available
        props.getProperty("startDate")?.let { sd ->
            startDate = LocalDate.parse(sd, DateTimeFormatter.ofPattern("dd.MM.yyyy"))
        }

        // 4. Set boolean attributes if available
        isVerbose = "true" == props.getProperty("verbose")
        enableFileCleanup = "true" == props.getProperty("enableFileCleanup")

        // 5. Set investAmount if available
        props.getProperty("investAmount")?.let { investAmount = Integer.valueOf(it) }

        // 6. Set tradeFee if available
        props.getProperty("tradeFee")?.let { tradeFee = java.lang.Double.valueOf(it) }

        // 6. Set kontoDir if available
        props.getProperty("kontoDir")?.let { kontoDirectory = File(it) }
    }

    fun printlnVerbose(text: Any) {
        if (isVerbose) {
            println(text)
        }
    }

    fun printVerbose(text: Any) {
        if (isVerbose) {
            print(text)
        }
    }
}
package nasahl.aktien

import java.time.LocalDate

class Trade(val depot: String, val datum: LocalDate, val isin: String, val name: String, val anzahl: Double, val kurs: Double) {

    override fun toString(): String {
        return "{depot='$depot', datum=$datum, isin='$isin', name='$name', anzahl=$anzahl, kurs=$kurs}"
    }

    // Gesamtkosten der Buchung
    fun totalCost(): Double {
        return anzahl * kurs
    }

    // Addiert eine Buchung zu this
    fun add(other: Trade): Trade {
        val neueAnzahl = anzahl + other.anzahl
        val neuerKurs = (totalCost() + other.totalCost()) / neueAnzahl
        return Trade(depot, datum, isin, name, neueAnzahl, neuerKurs)
    }

    // Berechnet einen eindeutigen Key
    fun uniqueKey(): String {
        return "${depot}-${datum}-${isin}" + if (anzahl > 0.0) "p" else "n"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Trade

        if (depot != other.depot) return false
        if (datum != other.datum) return false
        if (isin != other.isin) return false
        if (name != other.name) return false
        if (anzahl != other.anzahl) return false
        if (kurs != other.kurs) return false

        return true
    }

    override fun hashCode(): Int {
        var result = depot.hashCode()
        result = 31 * result + datum.hashCode()
        result = 31 * result + isin.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + anzahl.hashCode()
        result = 31 * result + kurs.hashCode()
        return result
    }


}

package nasahl.aktien

import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

/**
 * Writes the results
 */
class Writer internal constructor(config: Config) {
    private val lineList: MutableList<String> = ArrayList()
    private val outputFile: File
    private val sepLine = "------------------------------------------------------------------------------------------------------------"
    fun headline(vararg lines: String) {
        write(sepLine)
        for (line in lines) {
            write(line)
        }
        write(sepLine)
    }

    fun write(name: String?, rate: Double?, tas: Int, anzahl: Double,
              amount: Double, gewinn: Double) {
        val rateString = if (rate == null) "" else String.format("%.2f", rate)
        write(name, rateString, "$tas ", String.format("%.2f", anzahl), String.format("%.2f", amount), String.format("%.2f", gewinn))
    }

    fun write(text: String?, value: String?) {
        val line = String.format("%-92s %15s", text, value)
        write(line)
    }

    fun write(name: String?, rate: String?, tas: String?,
              anzahl: String?, amount: String?, gewinn: String?) {
        val line = String.format("%-40s %8s %10s %15s %15s %15s", name, tas, rate, anzahl, amount, gewinn)
        write(line)
    }

    fun writeEmpty() {
        write("")
    }

    @Throws(IOException::class)
    fun store() {
        if (outputFile.exists()) {
            outputFile.delete()
        }
        Files.newBufferedWriter(outputFile.toPath()).use { bw ->
            for (line in lineList) {
                bw.write("""
    $line
    
    """.trimIndent())
            }
        }
        println()
        println(sepLine)
        println("Result also written to " + outputFile.canonicalPath)
    }

    private fun write(line: String) {
        println(line)
        lineList.add(line)
    }

    init {
        val outputDir = File(config.shareDirectory, "output")
        outputDir.mkdir()
        outputFile = File(outputDir,
                LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd")) + "-output.txt")
    }
}
package nasahl.aktien

object Utils {
    @JvmStatic
            /** Converts double to string */
    fun d2s(value: Double): String {
        return String.format("%.2f", value)
    }


    @JvmStatic
            /** Converts string to double */
    fun s2d(s: String): Double {
        // In German csv files the floating number separator is ','
        return s.replace(',', '.').toDouble()
    }

}
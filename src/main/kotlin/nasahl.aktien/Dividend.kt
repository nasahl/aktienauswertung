package nasahl.aktien

import java.time.LocalDate

class Dividend(val datum: LocalDate, val betrag: String, val info: String)  {

    override fun toString(): String {
        return "{datum='$datum', betrag=$betrag, info='$info'}"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Dividend

        if (datum != other.datum) return false
        if (betrag != other.betrag) return false
        if (info != other.info) return false

        return true
    }

    override fun hashCode(): Int {
        var result = datum.hashCode()
        result = 31 * result + betrag.hashCode()
        result = 31 * result + info.hashCode()
        return result
    }
}

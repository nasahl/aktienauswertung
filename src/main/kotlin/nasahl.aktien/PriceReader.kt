package nasahl.aktien

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

class PriceReader {
    internal class ShareResponse {
        val price: Price? = null
    }

    class Price {
        @SerializedName("regularMarketOpen")
        val openPrice: RegularMarketPrice? = null

        @SerializedName("regularMarketPrice")
        val currentPrice: RegularMarketPrice? = null
    }

    class RegularMarketPrice {
        val raw: Double? = null
    }

    private val client = OkHttpClient()

    @Throws(IOException::class)
    fun getPrice(symbol: String): Price? {
        val request = Request.Builder()
                .url("https://apidojo-yahoo-finance-v1.p.rapidapi.com/stock/v2/get-summary?region=DE&symbol=$symbol")
                .get()
                .addHeader("x-rapidapi-host", "apidojo-yahoo-finance-v1.p.rapidapi.com")
                .addHeader("x-rapidapi-key", "6c45daf274msh75f58bb126965b3p189a63jsn4d2c7669b8a1")
                .build()
        val response = client.newCall(request).execute()
        //        System.out.println(response);
        check(response.isSuccessful) { "Response to yahoo api failed: ${response.message}" }
        val body = response.body
        val string = body!!.string()
        val gson = Gson()
        val shareResponse = gson.fromJson(string, ShareResponse::class.java)
                ?: throw IllegalStateException("No response data could be found for symbol '$symbol'")
        return shareResponse.price
    }

    companion object {
        @Throws(IOException::class)
        @JvmStatic
        fun main(args: Array<String>) {
//        String symbol = "WDI.DE"; // Wirecard
            val symbol = "OD7F.SG" // WITR COM.SEC.DZ06/UN.WTI
            val priceReader = PriceReader()
            val price = priceReader.getPrice(symbol)
            println("$symbol: open: ${price!!.openPrice!!.raw}; current: ${price.currentPrice!!.raw}")
        }
    }
}
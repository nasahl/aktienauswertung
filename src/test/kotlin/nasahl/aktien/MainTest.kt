package nasahl.aktien

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class MainTest {

    @Test
    fun testYearlyInterest() {

        assertEquals(100.0, Main.calculateYearlyInterest(5000, 10000.0, 1.0))
        assertEquals(50.0, Main.calculateYearlyInterest(20, 45.0, 2.0))
    }
}
package nasahl.aktien

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import java.time.LocalDate

internal class TradeTest {

    val trade1 = Trade("myDepot", LocalDate.now(), "isin1", "myShare", 10.0, 5.0)
    val trade2 = Trade("myDepot", LocalDate.now(), "isin1", "myShare", 10.0, 5.0)
    val trade3 = Trade("myDepot", LocalDate.now(), "isin1", "myShare", 8.0, 5.0)
    val trade4 = Trade("myDepot", LocalDate.now(), "isin2", "myShare", 10.0, 5.0)

    @Test
    fun testEquality() {
        assertEquals(trade1, trade2)
        assertNotEquals(trade1, trade3)
    }

    @Test
    fun testUniqueKey() {
        assertEquals(trade1.uniqueKey(), trade3.uniqueKey())
        assertNotEquals(trade1.uniqueKey(), trade4.uniqueKey())
    }
}